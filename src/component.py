'''
Template Component main class.

'''

import logging
import sys
import os  # noqa
import datetime  # noqa

from kbc.env_handler import KBCEnvHandler
from writer import Writer

from neo4j import GraphDatabase
from neo4j import basic_auth
from neo4j import kerberos_auth
from neo4j import custom_auth
from neo4j.exceptions import Neo4jError  # noqa
from neo4j.exceptions import DatabaseError
from neo4j.exceptions import ClientError
from neo4j.exceptions import TransientError
from neo4j.exceptions import DriverError


APP_VERSION = '0.0.1'
sys.tracebacklimit = 0
BATCH_PROCESSING_SIZE = 300000

# configuration variables
KEY_DEBUG = 'debug'
KEY_AUTH_TYPE = 'auth_type'
KEY_URL = 'url'
KEY_USERNAME = 'username'
KEY_PASSWORD = '#password'
KEY_SCHEME = 'scheme'

# row configuration variables
KEY_QUERY = 'query'
KEY_STORAGE_NAME = 'storage_name'
KEY_PRIMARY_KEY = 'primary_key'
KEY_INCREMENTAL = 'incremental'

MANDATORY_PARS = [
    KEY_AUTH_TYPE,
    KEY_URL,
    KEY_USERNAME,
    KEY_PASSWORD
]
MANDATORY_IMAGE_PARS = []


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        # Disabling list of libraries you want to output in the logger
        disable_libraries = [
            'neo4j'
        ]
        for library in disable_libraries:
            logging.getLogger(library).disabled = True

        logging.getLogger('neo4j').setLevel(logging.ERROR)

        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True

        log_level = logging.DEBUG if debug else logging.INFO
        # setup GELF if available
        if os.getenv('KBC_LOGGER_ADDR', None):
            self.set_gelf_logger(log_level)
        else:
            self.set_default_logger(log_level)

        try:
            self.validate_config()
        except ValueError as e:
            logging.error(e)
            exit(1)

    def validate_user_inputs(self, params):
        '''
        Validating all user entered parameters
        '''

        # 1 - Empty Configuration
        if not params:
            logging.error('Your configuration is missing.')
            sys.exit(1)

        # 2 - Validating if username, password and url are entered
        missing_info = []
        if not params[KEY_AUTH_TYPE]:
            missing_info.append('Authentication Type')
        if not params[KEY_USERNAME]:
            missing_info.append('User Name')
        if not params[KEY_PASSWORD]:
            missing_info.append('Password')
        if not params[KEY_URL] or params[KEY_URL] == 'bolt://':
            missing_info.append('URL')
        if missing_info:
            logging.error(f'Please enter your credentials: {missing_info}')
            sys.exit(1)

        # 3 - Check if scheme is configured
        # when custom authentication is selected
        if params['auth_type'] == 'custom':
            if not params['scheme']:
                logging.error('Authentication Scheme is missing.')
                sys.exit(1)

        # 4 - Check if the queries in the row
        if not params.get(KEY_QUERY) and not params.get(KEY_STORAGE_NAME):
            logging.error('Minimum one row configuration is required.')
            sys.exit(1)

        # 5 - Check if the row configuration contains the required param
        if not params.get(KEY_QUERY):
            missing_info.append('Cypher Query')
        # if not query_config['storage_name']:
        if not params.get(KEY_STORAGE_NAME):
            missing_info.append('Storage Output Destination')
        if missing_info:
            logging.error(f'Please configure your query: {missing_info}')
            sys.exit(1)

    def run(self):
        '''
        Main execution code
        '''

        # User input parameters
        params = self.cfg_params  # noqa

        # Validating user input
        self.validate_user_inputs(params)

        # Configuration inputs
        auth_type = params.get(KEY_AUTH_TYPE)
        url = params.get(KEY_URL)
        username = params.get(KEY_USERNAME)
        password = params.get(KEY_PASSWORD)
        scheme = params.get(KEY_SCHEME)

        # Row configuration input
        query = params.get(KEY_QUERY)
        output_table_name = f'{params.get(KEY_STORAGE_NAME)}.csv' if '.csv' not in params.get(
            KEY_STORAGE_NAME) else params.get(KEY_STORAGE_NAME)
        incremental = params.get(KEY_INCREMENTAL)
        primary_key = [v.get('value') for v in params.get(KEY_PRIMARY_KEY)]

        # Neo4j Table Writer
        self.writer = Writer(self.tables_out_path,
                             output_table_name, incremental, primary_key)

        # Neo4j Authentication
        try:
            # 1 - Basic Authentication
            if auth_type == 'basic':
                neo4j_auth = basic_auth(username, password)

            # 2 - Kerberos Authentication
            elif auth_type == 'kerberos':
                neo4j_auth = kerberos_auth(username, password)

            # 3 - Custom Authentication
            elif auth_type == 'custom':
                neo4j_auth = custom_auth(
                    username,
                    password,
                    realm=None,
                    schema=scheme
                )

            else:
                neo4j_auth = None

            neo4j_driver = GraphDatabase.driver(
                url,
                auth=neo4j_auth
            )
        except ClientError:
            logging.error(
                'Authentication Error. Please check your credentials')
            sys.exit(1)
        except DatabaseError:
            logging.error('Database encountered an error. Please try again.')
            sys.exit(1)
        except TransientError:
            logging.error('Please validate your access rights.')
            sys.exit(1)
        except Exception as err:
            logging.error(f'Connection Error: {err}')
            sys.exit(1)

        # Neo4j Session
        session = neo4j_driver.session()

        # Data return from Neo4j
        try:
            logging.info(f'Configured Cypher Code: {query}')
            data_in = session.run(query)
        # User input errors
        except ClientError:
            logging.error('Invalid Query. Please validate your Cypher Query.')
            sys.exit(1)
        except DatabaseError:
            logging.error('DatabaseError. Please try again.')
            sys.exit(1)
        except DriverError as err:
            logging.error(f'DriverError. Error message: {err}')
            sys.exit(1)
        except TransientError:
            logging.error('Please validate your access rights.')
            sys.exit(1)
        except Exception as err:
            logging.error(f'Error message: {err}')
            sys.exit(1)

        # Parsed Data list
        data_out = []
        processed_counter = 0

        for record in data_in:
            row = {}
            entity_queue = {}  # Captureing if there are any duplicated json objects

            for node in record:

                for obj in node:

                    if obj in entity_queue:
                        obj_name = f'{obj}_{entity_queue[obj]}'
                        row[obj_name] = node[obj]
                        entity_queue[obj] + 1

                    else:
                        row[obj] = node[obj]
                        entity_queue[obj] = 1

            data_out.append(row)
            processed_counter += 1

            if len(data_out) % BATCH_PROCESSING_SIZE == 0:
                self.writer.write_results(data_out, False)
                data_out = []
                logging.info(f'Process Counter: {processed_counter}')

        # Closing file IO
        logging.info(f'Processed [{processed_counter}] records.')
        self.writer.write_results(data_out, True)
        self.writer.create_manifest(
            self.writer.result_schema, self.writer.incremental, self.writer.primary_keys)

        logging.info("Neo4j Extraction finished")


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component(debug)
    comp.run()
