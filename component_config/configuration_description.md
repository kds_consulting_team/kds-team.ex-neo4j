Each row configuration is configured to extract data from one Cypher query and export the response into a pre-defined output table.

## Authentication

### Type
- Type of authenticatino supported for this component
- Available method
    1. [Basic Authentication](https://neo4j.com/docs/driver-manual/current/client-applications/#driver-authentication-basic)
        - User authenticate server using their user name and password.
        - This authentication scheme can be used to authenticate against LDAP server.
    2. [Kerberos Authentication](https://neo4j.com/docs/driver-manual/current/client-applications/#driver-authentication-kerberos)
        - User authenticate server by using a Kerberos authentication token with a base64 encoded server authentication ticket.
        - The Kerberos authentication token can only be understood by the server if the server has the Kerberos Add-on installed.
    3. [Custom Authentication](https://neo4j.com/docs/driver-manual/current/client-applications/#driver-authentication-custom)
        - This authentication method allows users to connect to any servers which are built with a custom security provider.
  
### Connection URI
- A connection URI identifies a graph database and how to connec to it.
- Typical URIs pattern
    ```
    neo4j://<HOST>:<PORT>[?<ROUTING_CONTEXT>]
    ```
- Available schemes below on how the connection URIs are formed

| URI Scheme | Routing | Description |
|-|-|-|
| `neo4j` | Yes | Unsecured |
| `neo4j+s` | Yes | Secured with full certificate |
| `neo4j+ssc` | Yes | Secured with self-signed certificate |
| `bolt` | No | Unsecured |
| `bolt+s` | No | Secured with full certificate |
| `bolt+ssc` | No | Secured with self-signed certificate |

### User Name / Principle
- Specifies who is being authenticated

### Credentials
- Credentials to authenticate the user name / principle

### Custom Authentication Scheme
- `Required` when [Custom Authentication] is selected
- Specifies the type of the authentication provider


## Row Configuration

### Cypher Query
- A query that retrieve sets of data that fall into the specified criteria.
- Example: `MATCH (n:Movie) RETURN n LIMIT 25` 
- To learn how to construct [Cyhper Query](https://neo4j.com/developer/cypher/#learn-cypher)

### Output Table Name
- The designated output table name in Keboola storage

### Primary Key
- Specifying a list of primary keys for the output table in Keboola

### Load Type
- Specifies whether to use `incremental load` or `full load` into Keboola storage