[Neo4j](https://neo4j.com/) is a native graph database, built from the ground up to leverage not only data but also data relationships. Neo4j connects data as it's stored, enabling queries never beofre imagined, at speed never thought possible.

This component allows users to extract data out of Neo4j using graph query language, Cypher.

For more information regarding Neo4j, please refer to [Neo4j developer](https://neo4j.com/developer/get-started/).